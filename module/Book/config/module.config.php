<?php
return [
    'service_manager' => [
        'factories' => [],
    ],
    'router' => [
        'routes' => [
            'book.rest.buku' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/buku[/:buku_id]',
                    'defaults' => [
                        'controller' => 'Book\\V1\\Rest\\Buku\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'api-tools-versioning' => [
        'uri' => [
            1 => 'book.rest.buku',
        ],
    ],
    'api-tools-rest' => [
        'Book\\V1\\Rest\\Buku\\Controller' => [
            'listener' => 'Book\\V1\\Rest\\Buku\\BukuResource',
            'route_name' => 'book.rest.buku',
            'route_identifier_name' => 'buku_id',
            'collection_name' => 'buku',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \Book\V1\Rest\Buku\BukuEntity::class,
            'collection_class' => \Book\V1\Rest\Buku\BukuCollection::class,
            'service_name' => 'buku',
        ],
    ],
    'api-tools-content-negotiation' => [
        'controllers' => [
            'Book\\V1\\Rest\\Buku\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'Book\\V1\\Rest\\Buku\\Controller' => [
                0 => 'application/vnd.book.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'Book\\V1\\Rest\\Buku\\Controller' => [
                0 => 'application/vnd.book.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'api-tools-hal' => [
        'metadata_map' => [
            \Book\V1\Rest\Buku\BukuEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'book.rest.buku',
                'route_identifier_name' => 'buku_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \Book\V1\Rest\Buku\BukuCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'book.rest.buku',
                'route_identifier_name' => 'buku_id',
                'is_collection' => true,
            ],
        ],
    ],
    'api-tools' => [
        'db-connected' => [
            'Book\\V1\\Rest\\Buku\\BukuResource' => [
                'adapter_name' => 'Book',
                'table_name' => 'buku',
                'hydrator_name' => \Laminas\Hydrator\ArraySerializable::class,
                'controller_service_name' => 'Book\\V1\\Rest\\Buku\\Controller',
                'entity_identifier_name' => 'id',
            ],
        ],
    ],
    'api-tools-content-validation' => [
        'Book\\V1\\Rest\\Buku\\Controller' => [
            'input_filter' => 'Book\\V1\\Rest\\Buku\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'Book\\V1\\Rest\\Buku\\Validator' => [
            0 => [
                'name' => 'judul',
                'required' => false,
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Laminas\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '65535',
                        ],
                    ],
                ],
            ],
            1 => [
                'name' => 'pengarang',
                'required' => false,
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Laminas\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '200',
                        ],
                    ],
                ],
            ],
            2 => [
                'name' => 'penerbit',
                'required' => false,
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Laminas\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '200',
                        ],
                    ],
                ],
            ],
            3 => [
                'name' => 'tahunTerbit',
                'required' => false,
                'filters' => [
                    0 => [
                        'name' => \Laminas\Filter\StringTrim::class,
                    ],
                    1 => [
                        'name' => \Laminas\Filter\StripTags::class,
                    ],
                ],
                'validators' => [
                    0 => [
                        'name' => \Laminas\Validator\StringLength::class,
                        'options' => [
                            'min' => 1,
                            'max' => '4',
                        ],
                    ],
                ],
            ],
        ],
    ],
];
